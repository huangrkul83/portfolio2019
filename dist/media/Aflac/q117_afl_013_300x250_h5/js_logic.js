    var startTime = 0, endTime = 0;

    function initAd() {
      //document.getElementById('bg-exit').addEventListener('click',bgExitHandler);
      // startTime = new Date();
      next = S1a;
      timeoutId = window.setTimeout(next, 1);
    }
    function S1a() {
      clearTimeout(timeoutId);
      Utils.removeClass("Slide1", "hide");
      next = S1b;
      timeoutId = window.setTimeout(next, 500);
    }
    function S1b() {
      clearTimeout(timeoutId);
      Utils.addAni("girl","girl1-inbottom");
      next = S1c;
      timeoutId = window.setTimeout(next, 250);
    }
    function S1c() {
      clearTimeout(timeoutId);
      Utils.addAni("chat1","chat1-pop");
      next = S1d;
      timeoutId = window.setTimeout(next, 1500);
    }
    function S1d() {
      clearTimeout(timeoutId);
      Utils.addAni("frame1","frame1-shift1");
      next = S2a;
      timeoutId = window.setTimeout(next, 100);
    }
    function S2a() {
      clearTimeout(timeoutId);
      Utils.addAni("duck","duck-inbottom");
      next = S2b;
      timeoutId = window.setTimeout(next, 250);
    }
    function S2b() {
      clearTimeout(timeoutId);
      Utils.addAni("chat2","chat2-pop");
      next = S2c;
      timeoutId = window.setTimeout(next, 2000);
    }
    function S2c() {
      clearTimeout(timeoutId);
      Utils.removeClass("chat2","chat2-pop");
      Utils.addClass("chat2","fadeout-quick");
      Utils.removeClass("chat2_v2","hide");
      next = S2d;
      timeoutId = window.setTimeout(next, 100);
    }
    function S2d() {
      clearTimeout(timeoutId);
      Utils.addClass("chat2_v2","chat2-shift1");
      next = S2e;
      timeoutId = window.setTimeout(next, 100);
    }
    function S2e() {
      clearTimeout(timeoutId);
      Utils.addAni("chat3","chat3-pop");
      next = S2f;
      timeoutId = window.setTimeout(next, 2200);
    }
    function S2f() {
      clearTimeout(timeoutId);
      Utils.removeClass("frame1","frame1-shift1");
      Utils.addClass("frame1","frame1-shift2");
      Utils.addClass("frame2","frame2-shift1");
      next = S3a;
      timeoutId = window.setTimeout(next, 300);
    }
    function S3a() {
      clearTimeout(timeoutId);
      Utils.addClass("frame1","hide");
      next = S3b;
      timeoutId = window.setTimeout(next, 10);
    }
    function S3b() {
      clearTimeout(timeoutId);
      Utils.addAni("girl2","girl2-inbottom");
      next = S3c;
      timeoutId = window.setTimeout(next, 250);
    }
    function S3c() {
      clearTimeout(timeoutId);
      Utils.addAni("chat4","chat4-pop");
      next = S3d;
      timeoutId = window.setTimeout(next, 2200);
    }
    function S3d() {
      clearTimeout(timeoutId);
      Utils.removeClass("frame2","frame2-shift1");
      Utils.addClass("frame2","frame2-shift2");
      Utils.addClass("frame3","frame3-shift");
      next = S4a;
      timeoutId = window.setTimeout(next, 100);
    }
    function S4a() {
      clearTimeout(timeoutId);
      Utils.addAni("bgImg","bg-shift");
      next = S4b;
      timeoutId = window.setTimeout(next, 200);
    }
    function S4b() {
      clearTimeout(timeoutId);
      Utils.addAni("duckend","inbottom");
      Utils.addAni("endmsg","inbottom");
      Utils.addAni("cta","inbottom");
      Utils.addAni("serial","fadein");
      next = S4c;
      timeoutId = window.setTimeout(next, 1000);
    }
    function S4c() {
      clearTimeout(timeoutId);
      Utils.removeClass("cta","inbottom");
      Utils.addClass("cta","cta-bump");
      next = finalFrame;
      timeoutId = window.setTimeout(next, 10);
    }
    function finalFrame() {
      clearTimeout(timeoutId);
      // endTime = new Date();
      // try {
      //   if (adCheckTime) {console.log(adCheckTime())};
      // } catch(e){}
    }
