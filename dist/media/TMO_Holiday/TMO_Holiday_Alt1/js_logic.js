var startTime = 0, endTime = 0;

function initAd() {
  startTime = new Date();
  next = logoStart;

  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);

}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated", "logo-animation-active");
  Utils.setAni("TMOLogoAnimated", 2200, "fadeout");
  Utils.setAni("magblur",2300,"fadein");
  Utils.setAni("light1",10,"fadein-out1");
  Utils.setAni("light2",200,"fadein-out2");
  Utils.setAni("light3",1000,"fadein-out3");
  Utils.setAni("light4",2000,"fadein-out2");
  next = F2A;
  timeoutId = window.setTimeout(next, 2200);
  pauseTime = window.setTimeout(pauseAll, 16000);
}

function F2A() {
  clearTimeout(timeoutId);
  Utils.removeClass("singleTLogo", "hide");
  Utils.setAni("msg1a",100,"inbottom");
  Utils.setAni("msg1b",100,"inbottom");
  Utils.setAni("msg1c",1200);
  Utils.setAni("msg1d",1500);
  Utils.setAni("msg1e",1700,"inbottom");
  next = F2B;
  timeoutId = window.setTimeout(next, 3200);
}

function F2B() {
  clearTimeout(timeoutId);
  Utils.setAni("msg1b",100,"outright");
  Utils.setAni("msg1c",200,"outright");
  Utils.setAni("msg1d",200,"outright");
  Utils.setAni("msg1e",300,"outright");

  Utils.setAni("msg1b_TMO",400,"inleft");
  Utils.setAni("msg1c2",500,"inleft");
  Utils.setAni("msg1e2",600,"inleft");
  Utils.setAni("msg1c2",1400,"makepop");

  next = F2C;
  timeoutId = window.setTimeout(next, 3500);
}

function F2C() {
  clearTimeout(timeoutId);
  Utils.setAni("msg1e2",10,"outright");
  Utils.setAni("msg1c2",100,"outright");
  Utils.setAni("msg1b_TMO",200,"outright");
  Utils.setAni("msg1a",300,"outright");
  Utils.setAni("msg2a",400,"inleft");
  Utils.setAni("msg2b",700,"fadezoombump");
  Utils.setAni("msg2c",1200,"makepop");
  next = F2D;
  timeoutId = window.setTimeout(next, 4000);
}

function F2D() {
  clearTimeout(timeoutId);
  Utils.setAni("msg2a",10,"outleft");
  Utils.setAni("msg2b",10,"outleft");
  Utils.setAni("msg2c",10,"outright");
  next = EFA;
  timeoutId = window.setTimeout(next, 400);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.setAni("phone1",10,"inright");
  Utils.setAni("phone2",100,"inright");
  Utils.setAni("phone3",200,"inright");
  Utils.setAni("phone4",300,"inright");
  Utils.setAni("phone_logo",300,"fadein");
  Utils.setAni("endmsg1",600,"inleft");
  Utils.setAni("endmsg2",600,"inleft");
  Utils.setAni("endmsg3",1000);
  Utils.setAni("endmsg_40",1200,"makepop-small");
  Utils.setAni("singleTLogo", 10, "hide");
  Utils.setAni("endLogo", 10, "show");
  Utils.setAni("CTA", 1800, "rotate-cta");
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}

function pauseAll() {
  document.getElementById("light1").style.animationPlayState = "paused";
  document.getElementById("light2").style.animationPlayState = "paused";
  document.getElementById("light3").style.animationPlayState = "paused";
  document.getElementById("light4").style.animationPlayState = "paused";
}
