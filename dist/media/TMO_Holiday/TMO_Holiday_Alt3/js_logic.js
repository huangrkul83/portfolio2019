var startTime = 0, endTime = 0;

function initAd() {
  startTime = new Date();
  next = logoStart;

  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);
}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated", "logo-animation-active");
  next = logoTransIn;
  timeoutId = window.setTimeout(next, 2000);
}

function logoTransIn() {
  clearTimeout(timeoutId);
  var caneInt = 50
  Utils.removeClass("caneTrans", "hide");
  Utils.setAni("cane1",10,"intop");
  Utils.setAni("cane2",caneInt * 1,"inbottom-quick");
  Utils.setAni("cane3",caneInt * 1.2,"intop");
  Utils.setAni("cane4",caneInt * 2.5,"inbottom-quick");
  Utils.setAni("cane5",caneInt * 4,"intop");
  Utils.setAni("cane6",caneInt * 4.3,"inbottom");
  Utils.setAni("cane7",caneInt * 6,"intop");
  Utils.setAni("cane8",caneInt * 7,"inbottom-quick");
  Utils.setAni("cane9",caneInt * 8.4,"intop");
  Utils.setAni("cane10",caneInt * 9.4,"inbottom");
  Utils.setAni("cane11",caneInt * 10,"intop");
  Utils.setAni("cane12",caneInt * 11,"inbottom");
  Utils.setAni("cane13",caneInt * 11.2,"intop-quick");
  Utils.setAni("cane14",caneInt * 13,"inbottom");
  Utils.setAni("cane15",caneInt * 14,"intop");
  next = logoTransOut;
  timeoutId = window.setTimeout(next, 1500);
}10

function logoTransOut() {
  clearTimeout(timeoutId);
  var caneInt = 50
  Utils.addClass("TMOLogo", "hide");
  Utils.removeClass("EF", "hide");
  Utils.setAni("cane1",10,"outtop");
  Utils.setAni("cane2",caneInt * 1,"outbottom");
  Utils.setAni("cane3",caneInt * 2,"outtop-quick");
  Utils.setAni("cane4",caneInt * 3,"outbottom");
  Utils.setAni("cane5",caneInt * 4,"outtop");
  Utils.setAni("cane6",caneInt * 5,"outbottom-quick");
  Utils.setAni("cane7",caneInt * 6,"outtop");
  Utils.setAni("cane8",caneInt * 7,"outbottom-quick");
  Utils.setAni("cane9",caneInt * 8,"outtop");
  Utils.setAni("cane10",caneInt * 9,"outbottom");
  Utils.setAni("cane11",caneInt * 10,"outtop");
  Utils.setAni("cane12",caneInt * 11,"outbottom-quick");
  Utils.setAni("cane13",caneInt * 12,"outtop-quick");
  Utils.setAni("cane14",caneInt * 13,"outbottom");
  Utils.setAni("cane15",caneInt * 14,"outtop-quick");
  next = EFA;
  timeoutId = window.setTimeout(next, 600);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide"); //failsafe for skipping
  Utils.addAni("phones", "inright");
  next = EFB;
  timeoutId = window.setTimeout(next, 300);
}

function EFB() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA", "rotate-cta");
  // Utils.addAni("EFLegal1", "fadein-quick");
  next = finalFrame;
  timeoutId = window.setTimeout(next, 250);
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}
