var startTime = 0, endTime = 0;

//vertical offset of snow particles in createRow() function.
var topOffset = 20;

//this will pause all tweenmax animation.
var isKilled = false;

//images of individual snow particles that will be semi-randomly placed in createRow() function.
var imgArray = [
	"snow1.png",
  "snow2.png",
  "snow3.png",
  "snow4.png",
  "snow5.png",
  "snow6.png",
  "snow7.png",
  "snow8.png"
];

var imgArray1 = [
	"flake1.png",
  "flake2.png",
  "flake3.png",
  "flake4.png",
  "flake5.png",
  "flake6.png"
];


function initAd() {
  startTime = new Date();
  next = logoStart;
	createRow("snowBox1","snow1",0,-50,300,100,imgArray);
  createRow("snowBox2","snow2",0,-50,300,20,imgArray1);
  createRow("snowBox3","snow3",0,-50,300,100,imgArray);
  createRow("snowBox4","snow4",0,-50,300,20,imgArray1);
	createRow("snowBox5","snow5",0,-50,300,100,imgArray);
	createRow("snowBox6","snow6",0,-50,300,20,imgArray1);
	Utils.addClass("snowBox1","fadein");
	Utils.addClass("snowBox2","fadein");
	Utils.addClass("snowBox3","fadein");
	Utils.addClass("snowBox4","fadein");
	Utils.addClass("snowBox5","fadein");
	Utils.addClass("snowBox6","fadein");
  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);
}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated","logo-animation-active");
	snowfall("snowBox1","snow1",10,0.3,10,-10,10,350,-1,true);
  snowfall("snowBox2","snow2",8.4,0.2,-10,10,-10,320,-1,true);
	snowfall("snowBox3","snow3",10.4,0.3,20,-20,20,340,-1,true);
  snowfall("snowBox4","snow4",8.8,0.2,-20,26,-20,350,-1,true);
	snowfall("snowBox5","snow5",6,0.6,50,-30,24,360,-1,true);
  snowfall("snowBox6","snow6",6.5,0.5,-50,30,-50,340,-1,true);
  next = F2A;
  timeoutId = window.setTimeout(next, 2000);
}

function F2A() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide");
  Utils.removeClass("logoSingleT", "hide");
  Utils.removeClass("F2", "hide");
	Utils.setAni("msg1a",100,"inbottom");
	Utils.setAni("msg1b",500,"zoomenter");
  next = F2B;
  timeoutId = window.setTimeout(next, 2000);
}

function F2B() {
  clearTimeout(timeoutId);
	Utils.setAni("msg1b",10,"outleft");
	Utils.setAni("msg1a",10,"outleft");
	Utils.setAni("img1",100,"inright");
	Utils.setAni("msg2a",400,"fadezoombump");
	Utils.setAni("msg2b",800,"inleft");
  next = F2C;
  timeoutId = window.setTimeout(next, 3000);
}

function F2C() {
  clearTimeout(timeoutId);
	Utils.setAni("img1",10,"outright");
	Utils.setAni("msg2a",10,"outleft");
	Utils.setAni("msg2b",10,"outleft");
  next = EFA;
  timeoutId = window.setTimeout(next, 80);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide"); //failsafe for skipping
  Utils.addClass("logoSingleT", "hide");
	// Utils.addClass("F2", "hide");
	Utils.removeClass("EF", "hide");
	Utils.setAni("snowBox1",2000,"fadeout");
	Utils.setAni("snowBox2",2000,"fadeout");
	Utils.setAni("snowBox3",2000,"fadeout");
	Utils.setAni("snowBox4",2000,"fadeout");
	Utils.setAni("snowBox5",2000,"fadeout");
	Utils.setAni("snowBox6",2000,"fadeout");
	Utils.setAni("end_snowbg",2000,"fadein-long");
	Utils.setAni("phone1",10,"inright");
	Utils.setAni("phone_logo",300,"fadein");
  Utils.setAni("endmsg1", 300, "fadezoombump");
	Utils.setAni("endmsg2", 600, "inbottom");
	Utils.setAni("endmsg3", 1000, "fadein");
  next = EFB;
  timeoutId = window.setTimeout(next, 1500);
}

function EFB() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA", "rotate-cta");
	Utils.addAni("endlegal", "fadein");
  // Utils.addAni("endlegal", "fadein-quick");
  next = finalFrame;
  timeoutId = window.setTimeout(next, 5000);
}

function finalFrame() {
  clearTimeout(timeoutId);
	isKilled = true;
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}


/////////////Snow generator script - Will Huang///////////////////
//Apology for the not-so-intuitive/performative script written last min for this project.
//Feel free to rewrite it however you see fit.
//I did my best integrating tweenmax so it will be more compatible on your (Toronto Team) end.  Cheers!
//description:
//this function essentially creates a row of randomly placed snow particles that can be positioned off screen
//in the beginning.
//particle's general size and opacity are handled by classes in compiler.css (.snow1 .snow2 .snow3 .snow4 etc)

//example: createRow("snowBox1","snow1",0,-50,300,50);
//parameters:
//1. divID: ID of the div container for one row. It's snowBox in the example file.
//2. itemClass: assign a class to each snow image in the row.  The class can then determine general image size and opacity.
//3. rowLeft: left position of the row.
//4. rowTop: top position of the row. (preferably off screen to avoid seeing it being generated)
//5. rowLenght: the length of the row where snow images will randomy be placed in.
//6. rowAmount: the amount of snow images that will populate the row div.
//7. imgArrVariable: this determines which img array to use for the snow images.  (currently snow and snowflakes)
function createRow(divID, itemClass, rowLeft, rowTop, rowLength, rowAmount, imgArrVariable) {
	//generate row div
	if(divID == undefined){console.log("missing div ID!");}
	if(itemClass == undefined){console.log("missing class!");}
	if(rowLeft == undefined){rowLeft = 0;}
	if(rowTop == undefined){rowTop = 1;}
	if(rowLength == undefined){rowLength = 300;}
	if(rowAmount == undefined){rowAmount = 10;}
	if(imgArrVariable == undefined){imgArrVariable = imgArray;}
  var divEle = document.getElementById(divID);
	var rowDiv = document.createElement('div');
	var k = 0; //variable k is used to make sure the randomness of the snow images is more spread out in the image array.
	rowDiv.style.position = "relative";
	rowDiv.style.left = rowLeft + "px";
	rowDiv.style.top = rowTop + "px";
	rowDiv.style.width = rowLength + "px";
	divEle.appendChild(rowDiv);

	//generate snow within row utilizing images from imgArray
	for (i = 0; i < rowAmount; i++) {
		var item = document.createElement('img');
		var randLeft = Math.floor(Math.random() * rowLength);
		var randTop = Math.floor(Math.random() * topOffset);
		// item.className = itemClass + " " + "hide";
    item.className = itemClass;
		item.style.position = "absolute";
    item.setAttribute("src", imgArrVariable[k]);
		item.style.left = randLeft + "px";
		item.style.top = randTop + "px";
		rowDiv.appendChild(item);
		k++;
		if(k > imgArrVariable.length -1) {
			k = 0;
		}
		// console.log(k);
	}
}

//description:
//this function will take the row div generated above and animate the snow particles within it using tweenMax cubic
//bezier feature (https://greensock.com/docs/Plugins/BezierPlugin) to simlulate snow fall.  Again, apology for the crazy amount of parameters!
//example: snowfall("snowBox3","snow3",2.4,0.3,1.8,50,-40,50,380,-1);
// parameters:
// 1. divID: the ID of the row div.
// 2. itemClass: the class of the snow images inside row div.
// 3. speed: fall speed (randomize per snow).
// 4. interval: interval between each snow fall (randomize per snow).
// 5. turbUpper: turbulence of the first bezier point. (it's really just the x position).  Y position is first quarter of the fall distance.
// 6.	turbLower: turbulence of the second bezier point. Y position is the third quarter of the fall distance.
// 7. endOffseX: this is the last x position where the snow will eventually land.
// 8. fallDistance: falling distance.  turbUpper and turbLower y positions depend on this value.
// 9. repeats: I have it set at -1 by default so the snowfall repeats indefinitely.
// 10. isInit: whether the snow animation should start midframe.
function snowfall(divID, itemClass, speed, interval, turbUpper, turbLower, endOffsetX, fallDistance, repeats, isInit) {
	if(divID == undefined){console.log("missing div ID!");}
	if(itemClass == undefined){console.log("missing class!");}
	if(speed == undefined){speed = 2;}
	if(interval == undefined){interval = 0.3;}
	if(turbUpper == undefined){turbUpper = 50;}
	if(turbLower == undefined){turbLower = -50;}
	if(endOffsetX == undefined){endOffsetX = 50;}
	if(fallDistance == undefined){fallDistance = 300;}
	if(repeats == undefined){repeats = -1;}
	if(isInit == undefined){isInit = false;}
	var divEle = document.getElementById(divID);
  var divItems = document.getElementsByClassName(itemClass);


  for (var i = 0; i < divItems.length; i++) {
    var mid1offsetX = turbUpper + Math.random() * 4;
		var mid2offsetX = turbLower + Math.random() * 4;
    var duration = speed + Math.random() * 4;
    var randDelay = isInit ? 0 : Math.random() * interval;
		var mid1Ypos = fallDistance / 4 + Math.random() * 8;
		var mid2Ypos = (fallDistance / 2) + (fallDistance / 4) + Math.random() * 8;
		var randProgress = isInit ? interval + Math.random() * 4 : 0;
		//tweenMax cubic bezier
    TweenMax.to(divItems[i], duration,
    {
      bezier:{
				type: "soft",
        values:[{x:mid1offsetX, y:mid1Ypos}, {x:mid2offsetX, y:mid2Ypos}, {x:endOffsetX, y:fallDistance}],
      },
      ease:Power2.easeInOut,
      repeat: repeats,
      repeatDelay: 1,
      delay: randDelay,
      onRepeat:checkRepeat
    }).progress(randProgress);
  }
}
///////////////////////////////// script end ///////////////////////////////



function checkRepeat() {
  if(isKilled){
		//in order to turn off infinite repeat, I had to replace the tween with new tween that basically do nothing.  Then pause all tweens
		//right after to stop it from moving.
    var timeoutSnow = setTimeout(function(){
      TweenMax.pauseAll();
    }, 10);
  }
}
