var startTime = 0, endTime = 0;
var timer = 1;
var bgMask = document.getElementById("maskRect");
var magMasked = document.getElementById("magMasked");

function politeLoadLucio() {
  var lucioContainer = document.getElementById("lucioContainer");
  var lucioSeq = document.createElement("div");
  lucioSeq.setAttribute('id','lucioSequence');
  lucioContainer.appendChild(lucioSeq);
}

function initAd() {
  document.getElementById('bg-exit').addEventListener('click',bgExitHandler);
  startTime = new Date();
  Utils.removeClass("bg","magenta");
  Utils.addClass("bg","black");
  // logoAnimate();
  politeLoadLucio();
  next = S1a;
  timeoutId = window.setTimeout(next, 10);
}

function S1a() {
  clearTimeout(timeoutId);
  Utils.removeClass("Slide1","hide");
  Utils.addAni("bar0","fadein");
  next = S1b;
  timeoutId = window.setTimeout(next, 2000);
}

function S1b() {
  clearTimeout(timeoutId);
  Utils.addClass("bar0","hide");
  Utils.removeClass("bar1","hide");
  next = msg1Reveal;
  timeoutId = window.setTimeout(next, 300);
}

function msg1Reveal() {
  clearTimeout(timeoutId);
  Utils.addAni("msg1","fadein");
  next = lucioSequence;
  timeoutId = window.setTimeout(next, 1500);
}

function S1c() {
  clearTimeout(timer);
  Utils.addClass("bar1","outleft-quick");
  Utils.removeClass("msg1","fadein");
  Utils.addClass("msg1","outleft-quick");
}

function lucioSequence() {
  clearTimeout(timeoutId);
  Utils.removeClass("lucioContainer","hide");
  Utils.addClass("lucioSequence","lucio-sequence");
  next = magWipe;
  timeoutId = window.setTimeout(next, 2200);
  timer = window.setTimeout(S1c, 200);
}

function magWipe() {
  clearTimeout(timeoutId);
  Utils.addAni("magWipe","mag-wipe");
  Utils.addClass("lucioContainer","screen-shake");
  next = initLogo;
  timeoutId = window.setTimeout(next, 450);
}

function initLogo() {
  Utils.addClass("lucioContainer","hide");
  Utils.addClass("lucioF1","hide");
  Utils.removeClass("TMOLogo","hide");
  Utils.removeClass("bg","black");
  Utils.addClass("bg","magenta");
  Utils.removeClass("logoShift1", "hide");
  Utils.addClass("logoShift1", "zoomenter");
  next = logoFinished;
  timeoutId = window.setTimeout(next, 2000);
}

function logoFinished() {
  clearTimeout(timeoutId);
  Utils.removeClass("logoShift1", "zoomenter");
  Utils.addClass("logoShift1", "outleft");
  next = S1d;
  timeoutId = window.setTimeout(next, 350);
}

function S1d() {
  clearTimeout(timeoutId);
  Utils.removeClass("logoShift2", "hide");
  Utils.addClass("logoShift1", "hide");
  Utils.addClass("magWipe","hide");
  Utils.addClass("Slide1","hide");
  Utils.removeClass("Slide2","hide");
  magMasked.setAttribute("x", 0);
  TweenLite.to(bgMask, 0, { ease:Power1.easeOut,
    attr:{x: 2000, y: 50,
    width: 364, height: 389},
  });  
  next = S2a;
  timeoutId = window.setTimeout(next, 10);
}

function S2a() {
  clearTimeout(timeoutId);
  Utils.addClass("magWipe","hide");
  Utils.addClass("Slide1","hide");
  Utils.removeClass("Slide2","hide");
  next = S2b;
  timeoutId = window.setTimeout(next, 150);
}

function S2b() {
  clearTimeout(timeoutId);
  TweenLite.to(bgMask, 0.6, { ease:Power2.easeOut,
    attr:{x: 607, y: 50,
    width: 364, height: 389},
  });
  next = S2c;
  timeoutId = window.setTimeout(next, 400);
}

function S2c() {
  clearTimeout(timeoutId);
  Utils.addAni("endmsg1","fadezoombump");
  next = S2d;
  timeoutId = window.setTimeout(next, 600);
}

function S2d() {
  clearTimeout(timeoutId);
  Utils.addAni("endmsg2","fadein-left");
  Utils.addAni("endlegal","fadein");
  next = S2e;
  timeoutId = window.setTimeout(next, 600);
}

function S2e() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA","rotate-cta");
  next = finalFrame;
  timeoutId = window.setTimeout(next, 10);
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime())};
  } catch(e){}
}

