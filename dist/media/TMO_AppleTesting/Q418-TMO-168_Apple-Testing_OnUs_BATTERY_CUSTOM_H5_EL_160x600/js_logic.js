var startTime = 0, endTime = 0;
var legalBtn = document.getElementById("legal_btn");
var legalFull = document.getElementById("legal_full");

function initAd() {
  startTime = new Date();
  next = logoStart;

  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);
}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated", "logo-animation-active");
  next = F2A;
  timeoutId = window.setTimeout(next, 2000);
}

function F2A() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide");
  Utils.removeClass("logoSingleT", "hide");
  Utils.removeClass("F2", "hide");
  Utils.setAni("phones",10,"inright");
  Utils.setAni("msg1a",300,"inleft");
  Utils.setAni("msg1b",600,"fadezoombump");
  next = F2B;
  timeoutId = window.setTimeout(next, 2500);
}

function F2B() {
  clearTimeout(timeoutId);
  Utils.setAni("msg1a",10,"outleft");
  Utils.setAni("msg1b",10,"outleft");
  Utils.setAni("phones",10,"outright");
  Utils.setAni("phone1",150,"inbottom-phone");
  Utils.setAni("msg2",750,"zoomenter");
  next = F2C;
  timeoutId = window.setTimeout(next, 1900);
}

function F2C() {
  clearTimeout(timeoutId);
  Utils.setAni("phone1",10,"fadeout");
  Utils.setAni("phone2",150,"fadein-slow");
  Utils.setAni("icon",200,"fadein");
  Utils.setAni("barEmpty",200,"fadein");
  Utils.setAni("bar",200,"fadein");
  Utils.setAni("bar",1000,"bar-charge");
  Utils.setAni("bar1",2200,"fadein");
  Utils.setAni("icon",2200,"fadeout");
  next = EFA;
  timeoutId = window.setTimeout(next, 4200);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide"); //failsafe for skipping
  Utils.addClass("logoSingleT", "hide");
  Utils.addClass("F2","hide");
  Utils.removeClass("EF", "hide");
  Utils.setAni("endPhones",10,"inright");
  Utils.setAni("endmsg1",300,"inleft");
  Utils.setAni("endmsg2",600,"zoomenter");
  Utils.setAni("endmsg3",1600,"rotate-cta");
  next = EFB;
  timeoutId = window.setTimeout(next, 2800);
}

function EFB() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA", "rotate-cta");
  Utils.addAni("endlegal", "fadein-quick");
  Utils.addAni("legal_btn");
  legalBtn.addEventListener("mouseover",legalIn);
  next = finalFrame;
  timeoutId = window.setTimeout(next, 250);
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}

//Rollover Legal Handlers
function legalIn() {
  Utils.removeClass("legal_full","hide");
  Utils.removeClass("legal_full","outbottom");
  Utils.addClass("legal_full","inbottom");

  legalBtn.addEventListener("mouseout",legalOut);
  legalBtn.addEventListener("click",legalStay);
}

function legalStay() {
  //swap click event attention to the rollover--turn pointer-events (mouse targeting) on
  legalFull.style.pointerEvents = "auto";
  Utils.addClass("legal_full","inbottom");
  legalFull.addEventListener("click",legalOut);

  legalBtn.removeEventListener("mouseout",legalOut)
  legalBtn.removeEventListener("click",legalStay);
}


function legalOut() {
  //Turn rollover click events back off and get it out of there!
  legalFull.removeEventListener("click",legalOut);
  legalFull.style.pointerEvents = "none";
  Utils.removeClass("legal_full","inbottom");
  Utils.addClass("legal_full","outbottom");

  legalBtn.addEventListener("mouseover",legalIn);
}
