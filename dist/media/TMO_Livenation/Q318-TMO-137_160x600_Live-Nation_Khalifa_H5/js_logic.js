var startTime = 0, endTime = 0;

function initAd() {
  startTime = new Date();
  next = logoStart;

  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);
}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated", "logo-animation-active");
  next = F2A;
  timeoutId = window.setTimeout(next, 2000);
}

function F2A() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide");
  Utils.removeClass("logoSingleT", "hide");
  Utils.removeClass("F2", "hide");
  Utils.addAni("msg1","inleft");
  Utils.addAni("msg2","inright");
  next = F2B;
  timeoutId = window.setTimeout(next, 2400);
}

function F2B() {
  clearTimeout(timeoutId);
  Utils.removeClass("img1","hide");
  Utils.addClass("msg1","outright");
  Utils.addClass("msg2","outleft");
  Utils.setAni("magbox",100,"outright");
  Utils.setAni("artist_main",300,"inleft");
  next = F2C;
  timeoutId = window.setTimeout(next, 3000);
}

function F2C() {
  clearTimeout(timeoutId);
  Utils.addClass("artist_main","outright");
  Utils.setAni("magbox",10,"abs-00 inleft","resetClass");
  Utils.setAni("artist1",250,"inleft");
  Utils.setAni("artist2",350,"inleft");
  Utils.setAni("artist3",450,"inleft");
  Utils.setAni("artist4",550,"inleft");
  Utils.setAni("andmore",650,"inleft");
  next = F2D;
  timeoutId = window.setTimeout(next, 3500);
}

function F2D() {
  clearTimeout(timeoutId);
  Utils.addClass("img1","hide");
  Utils.removeClass("img2","hide");
  Utils.setAni("artist1",50,"outright");
  Utils.setAni("artist2",100,"outleft");
  Utils.setAni("artist3",150,"outright");
  Utils.setAni("artist4",200,"outleft");
  Utils.setAni("andmore",250,"outright");
  Utils.setAni("magbox",300,"outleft");
  Utils.setAni("msg3",500,"fadezoombump");
  next = EFA;
  timeoutId = window.setTimeout(next, 2500);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide"); //failsafe for skipping
  Utils.addClass("logoSingleT", "hide");
  Utils.addClass("F2", "hide");
  Utils.removeClass("EF", "hide");
  Utils.setAni("endmsg1",100,"makepop");
  Utils.setAni("endmsg2",500,"inleft");
  Utils.setAni("endlegal",700,"fadein-quick");
  next = EFB;
  timeoutId = window.setTimeout(next, 900);
}

function EFB() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA", "rotate-cta");
  next = finalFrame;
  timeoutId = window.setTimeout(next, 250);
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}
