var startTime = 0, endTime = 0;

function initAd() {
  startTime = new Date();
  next = logoStart;

  // next = F2A; // for skipping to other frame
  timeoutId = window.setTimeout(next, 5);
}

function logoStart() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogoAnimated", "logo-animation-active");
  next = F2A;
  timeoutId = window.setTimeout(next, 2000);
}

function F2A() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide");
  Utils.removeClass("logoSingleT", "hide");
  Utils.removeClass("F2", "hide");
  Utils.addAni("msg1","intop");
  Utils.addAni("msg2","inbottom");
  next = F2B;
  timeoutId = window.setTimeout(next, 2400);
}

function F2B() {
  clearTimeout(timeoutId);
  Utils.addClass("msg1","outtop");
  Utils.addClass("msg2","outtop");
  Utils.addClass("magbox","outtop");
  Utils.setAni("img1",300,"inbottom");
  Utils.setAni("artist_main",300,"inbottom");
  next = F2C;
  timeoutId = window.setTimeout(next, 3000);
}

function F2C() {
  clearTimeout(timeoutId);
  Utils.addClass("artist_main","outtop");
  Utils.addClass("img1","outright");
  Utils.setAni("magbox",200,"abs-00 inbottom","resetClass");
  Utils.setAni("artist1",250,"inbottom");
  Utils.setAni("artist2",350,"inbottom");
  Utils.setAni("artist3",450,"inbottom");
  Utils.setAni("artist4",550,"inbottom");
  Utils.setAni("andmore",650,"inbottom");
  next = F2D;
  timeoutId = window.setTimeout(next, 3500);
}

function F2D() {
  clearTimeout(timeoutId);
  Utils.addClass("img1","hide");
  Utils.removeClass("img2","hide");
  Utils.setAni("artist1",100,"outtop");
  Utils.setAni("artist2",100,"outtop");
  Utils.setAni("artist3",100,"outtop");
  Utils.setAni("artist4",150,"outbottom");
  Utils.setAni("andmore",150,"outbottom");
  Utils.setAni("magbox",250,"outleft");
  Utils.setAni("msg3",500,"fadezoombump");
  next = EFA;
  timeoutId = window.setTimeout(next, 2500);
}

function EFA() {
  clearTimeout(timeoutId);
  Utils.addClass("TMOLogo", "hide"); //failsafe for skipping
  Utils.addClass("logoSingleT", "hide");
  Utils.addClass("F2", "hide");
  Utils.removeClass("EF", "hide");
  Utils.setAni("endmsg1",100,"makepop");
  Utils.setAni("endmsg2",500,"inbottom");
  Utils.setAni("endlegal",700,"fadein-quick");
  next = EFB;
  timeoutId = window.setTimeout(next, 900);
}

function EFB() {
  clearTimeout(timeoutId);
  Utils.addAni("CTA", "rotate-cta");
  next = finalFrame;
  timeoutId = window.setTimeout(next, 250);
}

function finalFrame() {
  clearTimeout(timeoutId);
  endTime = new Date();
  try {
    if (adCheckTime) {console.log(adCheckTime(startTime, endTime))};
  } catch(e){}
}
