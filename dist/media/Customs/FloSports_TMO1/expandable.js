//main variables

//===== Variables governing expanding dimensions =====>
// Please note Sass variables: `./sass/_variable_overrides.scss` should be adjusted as well.
//collapsed state width and height
var colWidth = 970;
var colHeight = 66;

//expanded state width and height
var expWidth = 970;
var expHeight = 300;

//resolution state width and height
var resWidth = 970;
var resHeight = 66;
//Set expanded offsets explicitly (Enabler doesn't like being passed in variables)
function setExpandingInit(){
  Enabler.setExpandingPixelOffsets(0,0,970,300);
}

//auto collapse and expanding variables
var isExpanded = false;
var isAutoExpand = false;
var timer1 = 0;

//DOM Elements
var colContainer = document.getElementById("collapsed-container");
var expContainer = document.getElementById("expanded-container");
var resContainer = document.getElementById("resolute-container");
var expandBtn = document.getElementById("expand-button");
var closeBtn = document.getElementById("close-button");
var opaWrap = document.getElementById("opa-wrap");
var bgExit = document.getElementById("bg-exit");
var playBtn = document.getElementById('play-btn');
var pauseBtn = document.getElementById('pause-btn');
var muteBtn = document.getElementById('mute-btn');
var unmuteBtn = document.getElementById('unmute-btn');
var replayBtn = document.getElementById('replay-btn');
var video1 = document.getElementById('video1');
var poster = document.getElementById('video-poster');

//additional elements

//detect whether user is using iOS.  This will dictate whether the video auto play or not.
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

//the entire thing is hidden until initiAd()
opaWrap.style.visibility = 'hidden';

//define width and height of all containers
colContainer.style.width = colWidth + "px";
colContainer.style.height = colHeight + "px";

expContainer.style.width = expWidth + "px";
expContainer.style.height = expHeight + "px";

resContainer.style.width = resWidth + "px";
resContainer.style.height = resHeight + "px";

//bgExit width height corresponds to various states.  Starts as collapsed width and height
bgExit.style.width = colContainer.style.width;
bgExit.style.height = colContainer.style.height;

///////////////window onload and enabler handling///////////////////
//* Function cascades from bottom to top
function adVisibilityHandler() {
  //Calls to functions that polite load other assets (images, fonts, etc.) would go here

  //initTimer is set to compensate for some publishers not loading the pushdown on time, resulting in pre-mature auto collapses.
  var initTimer = 1
  initTimer = setTimeout(initAd, 1000);
}

function pageLoadedHandler() {
  if (Enabler.isVisible()) {
    adVisibilityHandler()
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler)
  }
}


function enablerInitHandler() {
  if (Enabler.isPageLoaded()) {
    pageLoadedHandler()
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler)
  }
}

function preInit(){
  if (Enabler.isInitialized()) {
    enablerInitHandler();
    // console.log("initiated");
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
  }
}
///////////////window onload and enabler handling END///////////////////

//GLORIOUS INIT FUNCTION
function initAd() {
  //set the position and size for the expanded state -- see function set up above
  // setExpandingInit();

  //setStartExpanded does not seem to work either way...but I kept it and I don't know why?
  // Enabler.setStartExpanded(false);

  //make all collapsed state elements visible
  opaWrap.style.visibility = 'visible';
  expandBtn.style.visibility = 'visible';
  closeBtn.style.visibility = 'hidden';
  bgExit.style.visibility = 'visible';
  colContainer.style.visibility = 'visible';
  expContainer.style.visibility = 'hidden';
  resContainer.style.visibility = 'hidden';
  poster.style.visibility = 'hidden';

  //add all the event listeners for buttons and etc.
  expandBtn.addEventListener('click',expandClickHandler,false);
  closeBtn.addEventListener('click',closeClickHandler,false);
  bgExit.addEventListener('click',bgExitHandler, false);

  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START,expandStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH,expandFinishHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START,collapseStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH,collapseFinishHandler);

  playBtn.addEventListener('click', pausePlayHandler, false);
  pauseBtn.addEventListener('click', pausePlayHandler, false);
  muteBtn.addEventListener('click', muteUnmuteHandler, false);
  unmuteBtn.addEventListener('click', muteUnmuteHandler, false);
  replayBtn.addEventListener('click', replayHandler, false);
  expContainer.addEventListener('click', stopTimer, false);

  Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
  studio.video.Reporter.attach('video_1', video1);
  });

  //initiate auto expand
  // Enabler.requestExpand();
  // Enabler.counter("unit expanded");

  //timer for auto collapse
  // timer1 = setTimeout(closeClickHandler, 8000);

  //collapse state animation
}

//clicking anywhere on the expanded state will stop the auto collapse timer
function stopTimer(e) {
  clearTimeout(timer1);
}

//DC exit handler
//expected behavior: when bgexit is clicked, the unit collapses. (hide all buttons and vid)
function bgExitHandler(e) {
  Enabler.exit("Background Exit")
  if(isExpanded){
    clearTimeout(timer1);
    Enabler.requestCollapse();
    video1.pause();
    playBtn.style.visibility = 'hidden';
    pauseBtn.style.visibility = 'hidden';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'hidden';
    replayBtn.style.visibility = 'hidden';
  }
}

function expandStartHandler() {
  // Perform expand animation if there is any.
  // When animation finished must call
  Enabler.finishExpand();
  colContainer.style.visibility = 'hidden';
  expContainer.style.visibility = 'visible';
  resContainer.style.visibility = 'hidden';
  bgExit.style.width = expContainer.style.width;
  bgExit.style.height = expContainer.style.height;

  if(isAutoExpand){
    muteUnmuteHandler();
  }
}

function expandFinishHandler() {
  // Convenience callback for setting
  // final state when expanded.
  isExpanded = true;
  video1.style.visibility = 'visible';

  //video autoplay
  if(!iOS){
    expandBtn.style.visibility = 'hidden';
    closeBtn.style.visibility = 'visible';
    playBtn.style.visibility = 'hidden';
    pauseBtn.style.visibility = 'visible';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
    replayBtn.style.visibility = 'visible';
    video1.volume = 1;
    video1.currentTime = 0;
    video1.play();
  }

  //video does not autoplay
  if(iOS){
    expandBtn.style.visibility = 'hidden';
    closeBtn.style.visibility = 'visible';
    playBtn.style.visibility = 'visible';
    pauseBtn.style.visibility = 'hidden';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
    replayBtn.style.visibility = 'visible';
  }

  video1.addEventListener('ended', videoEndHandler, false);
}

function videoEndHandler(e) {
  video1.load();
  // poster.style.visibility = 'visible';
  video1.volume = 1.0;
  pauseBtn.style.visibility = 'hidden';
  playBtn.style.visibility = 'visible';
  muteBtn.style.visibility = 'hidden';
  unmuteBtn.style.visibility = 'visible';
}

function collapseStartHandler() {
  // Perform collapse animation.
  // When animation finished must call
  if(isAutoExpand){
    colContainer.style.visibility = 'visible';
    resContainer.style.visibility = 'hidden';

  } else {
    colContainer.style.visibility = 'hidden';
    resContainer.style.visibility = 'visible';
  }
  poster.style.visibility = 'hidden';
  expContainer.style.visibility = 'hidden';
  Enabler.finishCollapse();
  bgExit.style.width = resContainer.style.width;
  bgExit.style.height = resContainer.style.height;
}

function collapseFinishHandler() {
  // Convenience callback for setting
  // final state when collapsed.
  isExpanded = false;
  video1.style.visibility = 'hidden';
  closeBtn.style.visibility = 'hidden';

  if(isAutoExpand){
    isAutoExpand = false;
    expandBtn.style.visibility = 'visible';
  } else {
    expandBtn.style.visibility = 'hidden';
  }
}

function expandClickHandler() {
  if(!isExpanded){
    Enabler.requestExpand();
    Enabler.counter("unit expanded");
  }
}

function closeClickHandler() {
  if(isExpanded){
    clearTimeout(timer1);
    Enabler.requestCollapse();
    Enabler.reportManualClose();
    video1.pause();
    playBtn.style.visibility = 'hidden';
    pauseBtn.style.visibility = 'hidden';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'hidden';
    replayBtn.style.visibility = 'hidden';
  }
}

function pausePlayHandler(e) {
  Enabler.counter("pause/play clicked");
  poster.style.visibility = 'hidden';
  if (video1.paused) {
    // If paused, then play
    video1.play();
    // Show pause button and hide play button
    pauseBtn.style.visibility = 'visible';
    playBtn.style.visibility = 'hidden';
  } else {
    // If playing, then pause
    video1.pause();
    // Show play button and hide pause button
    pauseBtn.style.visibility = 'hidden';
    playBtn.style.visibility = 'visible';
   }
}

function muteUnmuteHandler(e) {
  Enabler.counter("mute/unmute clicked");
   if (video1.volume == 0.0) {
    // If muted, then turn it on
    video1.volume = 1.0;
    // Show mute button and hide unmute button
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
   } else {
    // If unmuted, then turn it off
    video1.volume = 0.0;
    // Show unmute button and hide mute button
    muteBtn.style.visibility = 'visible';
    unmuteBtn.style.visibility = 'hidden';
   }
}

function replayHandler(e) {
  // There is no replay method for HTML5 video
  // As a workaround, set currentTime to 0
  // and play the video
  Enabler.counter("replay clicked");
  poster.style.visibility = 'hidden';
  video1.currentTime = 0;
  video1.play();
  playBtn.style.visibility = 'hidden';
  pauseBtn.style.visibility = 'visible';
  // Show or hide other video buttons accordingly
}
