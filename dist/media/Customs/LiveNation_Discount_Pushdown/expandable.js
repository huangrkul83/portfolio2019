//Expanding and collapse dimensions set in Index to avoid DC issues looking for Enabler.setExpandingPixelOffsets method

//DOM Elements
var colContainer = document.getElementById('collapsed-container');
var expContainer = document.getElementById('expanded-container');
var resContainer = document.getElementById('resolute-container');
var expImgHolder = document.getElementById('expandImgHolder');
var expandBtn = document.getElementById('expand-button');
var closeBtn = document.getElementById('close-button');
var opaWrap = document.getElementById('opa-wrap');
var bgExit = document.getElementById('bg-exit');

//additional variables
var isExpanded = false;
var timer1 = 0;
var firstPlay = true;
var timeoutId = 0;

//video Elements
var vidCont = document.getElementById('videoContainer');
var playBtn = document.getElementById('playBtn');
var pauseBtn = document.getElementById('pauseBtn');
var muteBtn = document.getElementById('muteBtn');
var unmuteBtn = document.getElementById('unmuteBtn');
var replayBtn = document.getElementById('replayBtn');
var poster = document.getElementById('video-poster');
var initPlay = document.getElementById('initPlayBtn');
var vidBar = document.getElementById('progressBar');


//video and its sources will be added dynamically if true
function politeLoadVideo(){
  if(isVideoUnit){
    var video1 = document.createElement('video');
    video1.id = 'video1';
    video1.src = Enabler.getUrl('video.mp4');
    vidCont.appendChild(video1);
    var source = document.createElement('source');
    source.type = 'video/mp4';
    source.src = Enabler.getUrl('video.mp4');
    var source1 = document.createElement('source');
    source.type = 'video/ogg';
    source.src = Enabler.getUrl('video.ogg');
    var source2 = document.createElement('source');
    source.type = 'video/webm';
    source.src = Enabler.getUrl('video.webm');
    video1.appendChild(source);
    video1.appendChild(source1);
    video1.appendChild(source2);

    var supportsProgress = (document.createElement('progress').max !== undefined);

    if(showVidBar && supportsProgress){
      var progressBar = document.createElement('progress');
      progressBar.min = 0;
      progressBar.max = 100;
      progressBar.value = 0;
      progressBar.style.width = video1.offsetWidth + 'px';
      progressBar.style.left = video1.offsetLeft + 'px';
      progressBar.style.top = (video1.offsetTop + video1.offsetHeight) - progressBar.offsetHeight + 'px';
      vidBar.appendChild(progressBar);
    }

  } else {
    playBtn.classList.remove('play-btn');
    pauseBtn.classList.remove('pause-btn');
    muteBtn.classList.remove('mute-btn');
    unmuteBtn.classList.remove('unmute-btn');
    replayBtn.classList.remove('replay-btn');
  }

  //Create Youtube Video unit
  if(isYTVideo){
    // Load the IFrame Player API code asynchronously.
    var ytVideo = document.createElement('div');
    ytVideo.id = 'ytVideo';
    vidCont.appendChild(ytVideo);
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // Replace the ytVideo element with an <iframe> and
    // YouTube player after the API code downloads.
    var ytplayer;
    function onYouTubePlayerAPIReady() {
        ytplayer = new YT.Player('ytVideo', {
        width: '384',
        height: '216',
        videoId: '_SonP0OuKrM',
        //for more on ytplayer parameters: https://developers.google.com/youtube/player_parameters
        playerVars: {
          showinfo: '0',
          autoplay: '1',
          rel: '0'
        },
        events: {
          onReady: function(e) {
            e.target.mute();
          }
        }
      });
    }
  }
}

//detect whether user is using iOS.  This will dictate whether the video auto play or not.
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

//the entire thing is hidden until initiAd()
opaWrap.style.visibility = 'hidden';

//define width and height of all containers
colContainer.style.width = colWidth + "px";
colContainer.style.height = colHeight + "px";

expContainer.style.width = expWidth + "px";
expContainer.style.height = expHeight + "px";

resContainer.style.width = resWidth + "px";
resContainer.style.height = resHeight + "px";

//bgExit width height corresponds to various states.  Starts as collapsed width and height
bgExit.style.width = colContainer.style.width;
bgExit.style.height = colContainer.style.height;

///////////////window onload and enabler handling///////////////////
//* Function cascades from bottom to top
function adVisibilityHandler() {
  //Calls to functions that polite load other assets (images, fonts, etc.) would go here

  //initTimer is set to compensate for some publishers not loading the pushdown on time, resulting in pre-mature auto collapses.
  var initTimer = 1
  initTimer = setTimeout(initAd, 1000);
  politeLoadExpand();
  politeLoadVideo();
}

function pageLoadedHandler() {
  if (Enabler.isVisible()) {
    adVisibilityHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler)
  }
}

function enablerInitHandler() {
  if (Enabler.isPageLoaded()) {
    pageLoadedHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler)
  }
}

function preInit(){
  if (Enabler.isInitialized()) {
    enablerInitHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
  }
}
///////////////window onload and enabler handling END///////////////////

//GLORIOUS INIT FUNCTION
function initAd() {
  //set the position and size for the expanded state -- see function set up above
  setExpandingInit();

  //make all collapsed state elements visible
  opaWrap.style.visibility = 'visible';
  expandBtn.style.visibility = 'visible';
  closeBtn.style.visibility = 'hidden';
  bgExit.style.visibility = 'visible';
  colContainer.style.visibility = 'visible';
  expContainer.style.visibility = 'hidden';
  resContainer.style.visibility = 'hidden';
  poster.style.opacity = 0;
  initPlay.style.visibility = "hidden";

  //add all the event listeners for buttons and etc.
  expandBtn.addEventListener('click',expandClickHandler,false);
  closeBtn.addEventListener('click',closeClickHandler,false);
  bgExit.addEventListener('click',bgExitHandler, false);
  expContainer.addEventListener('click', stopTimer, false);

  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START,expandStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH,expandFinishHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START,collapseStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH,collapseFinishHandler);

  if(isVideoUnit) {
    playBtn.addEventListener('click', pausePlayHandler, false);
    pauseBtn.addEventListener('click', pausePlayHandler, false);
    muteBtn.addEventListener('click', muteUnmuteHandler, false);
    unmuteBtn.addEventListener('click', muteUnmuteHandler, false);
    replayBtn.addEventListener('click', replayHandler, false);

    if(enableInitBtn) {
      initPlay.addEventListener('click',pausePlayHandler, false);
    }

    if(showVidBar && supportsProgress) {
      video1.addEventListener('timeupdate', updateProgressBar, false);
      progressBar.addEventListener('click', onClickProgressBar, false);
    }

    Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
    studio.video.Reporter.attach('video_1', video1);
    });
  }

  //initiate auto expand
  if(isAutoExpand){
    Enabler.requestExpand();
    Enabler.counter("unit expanded");
    timer1 = setTimeout(closeClickHandler, 7000);
  }

  //collapse state animation
}

function politeLoadExpand() {
  var expContent = document.getElementById("expanded-content");
  var expandImg = document.createElement("img");
  expandImg.setAttribute('id','expand-img');
  expandImg.setAttribute('src', expandImgName);
  expImgHolder.appendChild(expandImg);
}

//clicking anywhere on the expanded state will stop the auto collapse timer
function stopTimer(e) {
  clearTimeout(timer1);
}

//DC exit handler
//expected behavior: when bgexit is clicked, the unit collapses. (hide all buttons and vid)
function bgExitHandler(e) {
  Enabler.exit("Background Exit")
  closeClickHandler();
}

function expandStartHandler() {
  // Perform expand animation if there is any.
  // When animation finished must call
  Enabler.finishExpand();
  colContainer.style.visibility = 'hidden';
  expContainer.style.visibility = 'visible';
  resContainer.style.visibility = 'hidden';
  bgExit.style.width = expContainer.style.width;
  bgExit.style.height = expContainer.style.height;
}

function expandFinishHandler() {
  // Convenience callback for setting
  // final state when expanded.
  isExpanded = true;
  expandBtn.style.visibility = 'hidden';
  closeBtn.style.visibility = 'visible';

  //video + autoplay logic
  if(isVideoUnit){

    //initial video setup
    firstPlay = true;
    video1.style.visibility = 'visible';
    video1.addEventListener('ended', videoEndHandler, false);
    video1.volume = 1;
    video1.currentTime = splashFrame;
    video1.pause();
    playBtn.style.visibility = 'visible';
    pauseBtn.style.visibility = 'hidden';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
    replayBtn.style.visibility = 'visible';
    initPlay.style.visibility = "visible";

    //autoplay video if it isn't an iOS device
    if(!iOS){
      if(isAutoExpand){
        //autoplay ALWAYS start muted
        muteUnmuteHandler();
      }
      //plays video
      if(isAutoPlay){
        pausePlayHandler();
      }
    }
  }
  //youtube player
  if(isYTVideo){
    if(isAutoPlay){
      ytplayer.playVideo();
    }
  }
  //animate elements in expanded state
  initAnimate();
}

function initAnimate(){
  clearTimeout(timeoutId);
  next = F1a;
  timeoutId = window.setTimeout(next, 300);
}

function F1a(){
  clearTimeout(timeoutId);
  Utils.addAni("msg1", "fadezoombump");
  next = F2a;
  timeoutId = window.setTimeout(next, 500);
}

function F2a(){
  clearTimeout(timeoutId);
  Utils.addAni("msg2a","inleft");
  Utils.addAni("msg2b","inright");
  next = F2b;
  timeoutId = window.setTimeout(next, 100);
}

function F2b(){
  clearTimeout(timeoutId);
  Utils.addAni("msg2c","makepop");
  next = F2c;
  timeoutId = window.setTimeout(next, 600);
}

function F2c(){
  clearTimeout(timeoutId);
  Utils.addAni("msg3","inbottom");
  next = EFA;
  timeoutId = window.setTimeout(next, 500);
}

function EFA(){
  clearTimeout(timeoutId);
  Utils.addAni("CTA","rotate-cta");

  next = EFB;
  timeoutId = window.setTimeout(next, 250);
}

function EFB(){
  clearTimeout(timeoutId);
  Utils.addAni("legal1","fadein-quick");
}


function videoEndHandler(e) {
  // video1.load();  //use when you want video to go back to frame 1 when it ends
  if(enablePoster){
    poster.style.opacity = 1;
  }
  video1.volume = 1.0;
  video1.pause();
  pauseBtn.style.visibility = 'hidden';
  playBtn.style.visibility = 'visible';
  muteBtn.style.visibility = 'hidden';
  unmuteBtn.style.visibility = 'visible';
}

function collapseStartHandler() {

  //stop all animation (if there is any)
  clearTimeout(timeoutId);
  Utils.resetClass("msg1","abs-00 hide");
  Utils.resetClass("msg2a","abs-00 hide");
  Utils.resetClass("msg2b","abs-00 hide");
  Utils.resetClass("msg2c","abs-00 hide");
  Utils.resetClass("msg3","abs-00 hide");
  Utils.resetClass("legal1","abs-00 hide");
  Utils.resetClass("CTA","retina-img-full abs-00 hide");

  // Perform collapse animation.
  // When animation finished must call
  if(isAutoExpand){
    colContainer.style.visibility = 'visible';
    resContainer.style.visibility = 'hidden';
    expandBtn.style.visibility = 'visible';
  } else {
    if(enableResolve){
      colContainer.style.visibility = 'hidden';
      resContainer.style.visibility = 'visible';
      expandBtn.style.visibility = 'hidden';
    } else {
      colContainer.style.visibility = 'visible';
      resContainer.style.visibility = 'hidden';
      expandBtn.style.visibility = 'visible';
    }
  }
  poster.style.opacity = 0;
  initPlay.style.visibility = "hidden";
  expContainer.style.visibility = 'hidden';
  Enabler.finishCollapse();
  bgExit.style.width = resContainer.style.width;
  bgExit.style.height = resContainer.style.height;
}

function collapseFinishHandler() {
  // Convenience callback for setting
  // final state when collapsed.
  isExpanded = false;
  closeBtn.style.visibility = 'hidden';

  if(isVideoUnit){
    video1.style.visibility = 'hidden';
  }
  if(isAutoExpand){
    isAutoExpand = false;
  }
}

function expandClickHandler() {
  if(!isExpanded){
    Enabler.requestExpand();
    Enabler.counter("unit expanded");
  }
}

function closeClickHandler() {
  if(isExpanded){
    clearTimeout(timer1);
    Enabler.requestCollapse();
    Enabler.reportManualClose();
    if(isVideoUnit){
      video1.pause();
      playBtn.style.visibility = 'hidden';
      pauseBtn.style.visibility = 'hidden';
      muteBtn.style.visibility = 'hidden';
      unmuteBtn.style.visibility = 'hidden';
      replayBtn.style.visibility = 'hidden';
    }
    if(isYTVideo){
      ytplayer.pauseVideo();
    }
  }
}

function pausePlayHandler(e) {
  Enabler.counter("pause/play clicked");
  poster.style.opacity = 0;
  if (firstPlay) {
    video1.currentTime = 0;
    firstPlay = false;
    initPlay.style.visibility = "hidden";
  }
  if (video1.paused) {
    // If paused, then play
    video1.play();
    // Show pause button and hide play button
    pauseBtn.style.visibility = 'visible';
    playBtn.style.visibility = 'hidden';
  } else {
    // If playing, then pause
    video1.pause();
    // Show play button and hide pause button
    pauseBtn.style.visibility = 'hidden';
    playBtn.style.visibility = 'visible';
   }
}

function muteUnmuteHandler(e) {
  Enabler.counter("mute/unmute clicked");
   if (video1.volume == 0.0) {
    // If muted, then turn it on
    video1.volume = 1.0;
    // Show mute button and hide unmute button
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
   } else {
    // If unmuted, then turn it off
    video1.volume = 0.0;
    // Show unmute button and hide mute button
    muteBtn.style.visibility = 'visible';
    unmuteBtn.style.visibility = 'hidden';
   }
}

function replayHandler(e) {
  // There is no replay method for HTML5 video
  // As a workaround, set currentTime to 0
  // and play the video
  Enabler.counter("replay clicked");
  poster.style.opacity = 0;
  video1.currentTime = 0;
  video1.play();
  playBtn.style.visibility = 'hidden';
  pauseBtn.style.visibility = 'visible';

  if(showVidBar && supportsProgress) {
    progressBar.value = 0;
  }
  // Show or hide other video buttons accordingly
}

function updateProgressBar() {
  var percentage = Math.floor((100 / video1.duration) *
  video1.currentTime);
  progressBar.value = percentage;
  progressBar.innerHTML = percentage + '% played';
}

function onClickProgressBar(e) {
  var x = e.pageX - video1.offsetLeft;
  video1.currentTime = (video1.duration / video1.offsetWidth) * x;
}
