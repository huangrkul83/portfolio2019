//Variables setting dimensions should be set on index to avoid errors involved in the DC Studio's search for "Enabler.setExpandingPixelOffsets" method while uploading

var leftOffset = 0,
    topOffset = 0,
    isExpanded = false,
    timeoutId = 0,
    isFirstExpand = true,

    colContainer = document.getElementById("collapsed-container"),
    colContent = document.getElementById("collapsed-content"),
    expContainer = document.getElementById("expanded-container"),
    expContent = document.getElementById("expanded-content"),
    resContainer = document.getElementById("resolved-container"),
    resContent = document.getElementById("resolve-content"),
    expandBtn = document.getElementById("expand-button"),
    closeBtn = document.getElementById("close-button"),
    adContainer = document.getElementById("ad-container"),
    bgExit = document.getElementById("bg-exit"),
    phoneSeq = document.getElementById("phoneSequence");

adContainer.style.visibility = "hidden", colContainer.style.width = colWidth + "px", colContainer.style.height = colHeight + "px", expContainer.style.width = expWidth + "px", expContainer.style.height = expHeight + "px", resContainer.style.width = resWidth + "px", resContainer.style.height = resHeight + "px";
var userExpanded = false;

window.addEventListener("load", preInit);


//Initiation Cascade
function initAd() {
    phoneSeq.style.backgroundImage = "url('phoneSequence.png')";
    setExpandingInit();
    Enabler.setStartExpanded(!1);
    adContainer.style.visibility = "visible";
    expandBtn.style.visibility = "visible";
    closeBtn.style.visibility = "hidden";
    bgExit.style.visibility = "hidden";
    colContainer.style.visibility = "visible";
    expContainer.style.visibility = "hidden";
    resContainer.style.visibility = "hidden";
    expandBtn.addEventListener("click", expandClickHandler, !1);
    closeBtn.addEventListener("click", manualCloseClickHandler, !1);
    document.getElementById("expandedCloseFallback").addEventListener("click", manualCloseClickHandler, !1);
    Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START, expandStartHandler);
    Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, expandFinishHandler);
    Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START, collapseStartHandler);
    Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH, collapseFinishHandler);
}

function expandClickHandler() {
    isExpanded || expandAd()
    if(isFirstExpand){
        S1a();
    } else {

        S1a();
    }
}

function expandAd(e) {
    Enabler.requestExpand(), Enabler.startTimer("Panel Expansion")
}

function expandStartHandler() {
    userExpanded !== !0 && (userExpanded = !0);
    expContainer.style.visibility = "visible";
    colContainer.style.visibility = "hidden";
    resContainer.style.visibility = "hidden";
    bgExit.style.visibility = "visible";
    bgExit.style.width = expWidth + "px";
    bgExit.style.height = expHeight + "px";
    expandBtn.style.visibility = "hidden";
    Enabler.finishExpand();
}

function expandFinishHandler() {
    closeBtn.style.visibility = "visible", isExpanded = !0
}

function S1a() {
    clearTimeout(timeoutId);
    Utils.addAni("ba01","top-settle");
    Utils.addAni("ba02","top-settle2");
    Utils.addAni("ba03","top-settle1");
    Utils.addAni("ba04","top-settle");
    Utils.addAni("ba05","top-settle1");
    Utils.addAni("ba06","top-settle2");

    Utils.addAni("btm01","btm-settle");
    Utils.addAni("btm02","btm-settle2");
    Utils.addAni("btm03","btm-settle1");
    Utils.addAni("btm04","btm-settle1");
    Utils.addAni("closebtn","fadein");
    next = S1b;
    timeoutId = window.setTimeout(next, 100);
}

function S1b() {
    clearTimeout(timeoutId);
    Utils.addAni("ba07","top-settle");
    Utils.addAni("ba08","top-settle1");
    Utils.addAni("ba09","top-settle2");
    Utils.addAni("ba10","top-settle");
    Utils.addAni("ba11","top-settle1");
    Utils.addAni("ba12","top-settle2");

    Utils.addAni("btm05","btm-settle");
    Utils.addAni("btm06","btm-settle1");
    Utils.addAni("btm07","btm-settle2");
    next = S2a;
    timeoutId = window.setTimeout(next, 400);
}

function S2a() {
    clearTimeout(timeoutId);
    Utils.addAni("msg1", "inleft");
    Utils.addAni("msg2a", "inleft");
    next = S2b;
    timeoutId = window.setTimeout(next, 800);
}

function S2b() {
    clearTimeout(timeoutId);
    Utils.addAni("msg2b", "zoomenter");
    Utils.addAni("msg2c", "zoomenter");
    next = S2b1;
    timeoutId = window.setTimeout(next, 400);
}

function S2b1() {
    clearTimeout(timeoutId);
    Utils.addAni("legal1", "fadein");
    next = S2c;
    timeoutId = window.setTimeout(next, 1000);
}

function S2c() {
    clearTimeout(timeoutId);
    Utils.removeClass("msg2b","zoomenter");
    Utils.removeClass("msg2c","zoomenter");
    Utils.addClass("msg2b", "makepop");
    Utils.addClass("msg2c", "makepop");
    next = S2d;
    timeoutId = window.setTimeout(next, 500);
}

function S2d() {
    clearTimeout(timeoutId);
    Utils.addClass("phoneSequence", "phone-sequence");
    next = S2e;
    timeoutId = window.setTimeout(next, 2000);
}

function S2e() {
    clearTimeout(timeoutId);
    Utils.addAni("CTA", "rotate-cta");
    Utils.addAni("endlegal", "fadein");
    // next = S2g;
    // timeoutId = window.setTimeout(next, 800);
}

function manualCloseClickHandler() {
    Enabler.reportManualClose(), closeClickHandler()
}

function closeClickHandler() {
    closeBtn.style.visibility = "hidden", isExpanded && (Enabler.requestCollapse(), Enabler.stopTimer("Panel Expansion"))
    Utils.resetClass("msg1", "expanded-content hide");
    Utils.resetClass("msg2a", "expanded-content hide");
    Utils.resetClass("msg2b", "expanded-content hide");
    Utils.resetClass("msg2c", "expanded-content hide");
    Utils.resetClass("legal1", "expanded-content hide");
    Utils.resetClass("endlegal", "expanded-content hide");
    Utils.resetClass("closebtn", "expanded-content hide");
    Utils.resetClass("CTA", "expanded-content hide");
    Utils.resetClass("ba01", "ba-01 hide abs-bauble");
    Utils.resetClass("ba02", "ba-02 hide abs-bauble");
    Utils.resetClass("ba03", "ba-03 hide abs-bauble");
    Utils.resetClass("ba04", "ba-04 hide abs-bauble");
    Utils.resetClass("ba05", "ba-05 hide abs-bauble");
    Utils.resetClass("ba06", "ba-06 hide abs-bauble");
    Utils.resetClass("ba07", "ba-07 hide abs-bauble");
    Utils.resetClass("ba08", "ba-08 hide abs-bauble");
    Utils.resetClass("ba09", "ba-09 hide abs-bauble");
    Utils.resetClass("ba10", "ba-10 hide abs-bauble");
    Utils.resetClass("ba11", "ba-11 hide abs-bauble");
    Utils.resetClass("ba12", "ba-12 hide abs-bauble");
    Utils.resetClass("btm01", "btm-01 hide abs-bauble");
    Utils.resetClass("btm02", "btm-02 hide abs-bauble");
    Utils.resetClass("btm03", "btm-03 hide abs-bauble");
    Utils.resetClass("btm04", "btm-04 hide abs-bauble");
    Utils.resetClass("btm05", "btm-05 hide abs-bauble");
    Utils.resetClass("btm06", "btm-06 hide abs-bauble");
    Utils.resetClass("btm07", "btm-07 hide abs-bauble");
    Utils.resetClass("phoneSequence", "");
}

function collapseStartHandler() {
    Enabler.finishCollapse()
}

function collapseFinishHandler() {
    expContainer.style.visibility = "hidden", isExpanded = !1, bgExit.style.visibility = "hidden", closeBtn.style.visibility = "hidden", colContainer.style.visibility = "visible", expandBtn.style.visibility = "visible"
    // Utils.resetClass("msg1", "expanded-content");
    // Utils.resetClass("msg2", "expanded-content");
    // Utils.resetClass("legal1", "expanded-content");
    // Utils.resetClass("endlegal", "expanded-content");
    // Utils.resetClass("phonelogo", "expanded-content");
    // Utils.resetClass("closebtn", "expanded-content");
    // Utils.resetClass("CTA", "expanded-content");
    // Utils.resetClass("phoneSequence", "phone-endframe");
    isFirstExpand = false;
    clearTimeout(timeoutId);
}

function adVisibilityHandler() {
    initAd()
}

function pageLoadedHandler() {
    document.getElementById("bg-exit").addEventListener("click", bgExitHandler, !1), Enabler.isVisible() ? adVisibilityHandler() : Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler)
}

function enablerInitHandler() {
    Enabler.isPageLoaded() ? pageLoadedHandler() : Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler)
}

function preInit() {
    Enabler.isInitialized() ? enablerInitHandler() : Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler)
}

function bgExitHandler(e) {
    closeClickHandler(), Enabler.exit("Background Exit")
}

