//Variables

//Expanding and collapse dimensions set in Index to avoid DC issues looking for Enabler.setExpandingPixelOffsets method

//auto collapse and expanding variables
var timer1 = 0;
var timeoutId;
var playFirstTime = true;

//DOM Elements
var bgExit = document.getElementById("bg-exit");
var playBtn = document.getElementById('play-btn');
var pauseBtn = document.getElementById('pause-btn');
var muteBtn = document.getElementById('mute-btn');
var unmuteBtn = document.getElementById('unmute-btn');
var replayBtn = document.getElementById('replay-btn');
var video1 = document.getElementById('video1');
var clickToPlay = document.getElementById('vid-border');
var vidDarkbg = document.getElementById('vid-darkbg');

//detect whether user is using iOS.  This will dictate whether the video auto play or not.
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

//GLORIOUS INIT FUNCTION
function initVideo() {

  playBtn.addEventListener('click', pausePlayHandler, false);
  pauseBtn.addEventListener('click', pausePlayHandler, false);
  muteBtn.addEventListener('click', muteUnmuteHandler, false);
  unmuteBtn.addEventListener('click', muteUnmuteHandler, false);
  replayBtn.addEventListener('click', replayHandler, false);
  clickToPlay.addEventListener('click', pausePlayHandler, false);

  Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
  studio.video.Reporter.attach('video_1', video1);
  });

  playBtn.style.visibility = 'visible';
  pauseBtn.style.visibility = 'hidden';
  muteBtn.style.visibility = 'hidden';
  unmuteBtn.style.visibility = 'visible';
  replayBtn.style.visibility = 'visible';

  video1.addEventListener('ended', videoEndHandler, false);
  
  if(!iOS){
    video1.currentTime = 0;
    video1.volume = 0;
    video1.play();
    playBtn.style.visibility = 'hidden';
    pauseBtn.style.visibility = 'visible';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
    replayBtn.style.visibility = 'visible';
  } else {
    video1.currentTime = 5;
    video1.volume = 1;
    video1.pause();
    playBtn.style.visibility = 'visible';
    pauseBtn.style.visibility = 'hidden';
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
    replayBtn.style.visibility = 'visible';
  }

}

function videoEndHandler(e) {
  // video1.load();
  video1.volume = 1.0;
  pauseBtn.style.visibility = 'hidden';
  playBtn.style.visibility = 'visible';
  muteBtn.style.visibility = 'hidden';
  unmuteBtn.style.visibility = 'visible';
}

function pausePlayHandler(e) {
  console.log(e);
  Enabler.counter("pause/play clicked");
  if (video1.paused) {
    // If paused, then play
    video1.play();
    // Show pause button and hide play button
    pauseBtn.style.visibility = 'visible';
    playBtn.style.visibility = 'hidden';
  } else {
    // If playing, then pause
    video1.pause();
    // Show play button and hide pause button
    pauseBtn.style.visibility = 'hidden';
    playBtn.style.visibility = 'visible';
   }
  if (playFirstTime) {
    video1.currentTime = 0;
    playFirstTime = false;
    vidDarkbg.style.opacity = 0;
    Utils.addClass("play-arrow", "hide");
  }
}

function muteUnmuteHandler(e) {
  Enabler.counter("mute/unmute clicked");
   if (video1.volume == 0.0) {
    // If muted, then turn it on
    video1.volume = 1.0;
    // Show mute button and hide unmute button
    muteBtn.style.visibility = 'hidden';
    unmuteBtn.style.visibility = 'visible';
   } else {
    // If unmuted, then turn it off
    video1.volume = 0.0;
    // Show unmute button and hide mute button
    muteBtn.style.visibility = 'visible';
    unmuteBtn.style.visibility = 'hidden';
   }
}

function replayHandler(e) {
  // There is no replay method for HTML5 video
  // As a workaround, set currentTime to 0
  // and play the video
  Enabler.counter("replay clicked");
  video1.currentTime = 0;
  video1.play();
  playBtn.style.visibility = 'hidden';
  pauseBtn.style.visibility = 'visible';
  // Show or hide other video buttons accordingly
}
